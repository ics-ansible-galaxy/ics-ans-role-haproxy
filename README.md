# ics-ans-role-haproxy

Ansible role to install haproxy.

## Role Variables

```yaml
Frontend services are defined in haproxy_front_services.
Related backend are defined in haproxy_back_services.

Fro LDAP, real servers are defined by haproxy_ldap_srv1 and haproxy_ldap_srv2
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-haproxy
```

## License

BSD 2-clause
